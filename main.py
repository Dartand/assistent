import pyttsx3
import speech_recognition
from datetime import datetime
import os
import random 
import webbrowser

opts = {
    "name" :("маша", "терминатор", "дубина", "маруся", "железка", "железяка", "юля", "юль", "боженька"),
    "bad_logs" : ("скажи", "произнеси", "расскажи", "покажи", "загрузи", "твою мать", "открой", "включи", "подскажи", "найди"),

    "hello" :("привет", "здарова", "доброе утро", "добрый день", "добрый вечер",
         "хаю хай"),
    "goodbuy":("покеда", "досвидание", "пока"),
    "time" :("который час", "сколько времени", "че по тайму", "время"),
    "anecdot" :("анекдот", "пошути", "шутку"),
    "youtybe" :("видео", "видосик"),
    "music" :("музыку"),
    "google" :()
}


tts = pyttsx3.init() 
voices = tts.getProperty('voices')
tts.setProperty('voice', voices[3].id)



def start(text, opts):
    print(text)
    if text.startswith(opts['name']):
        for x in opts['name']:
            text = text.replace(x, " ").strip()
        for x in opts['bad_logs']:
            text = text.replace(x, " ").strip() 
        hello(text, opts)
    else:
        speac("Вы ко мне обращаетесь?")

def speac(text):
    tts.say(text)
    tts.runAndWait()
    tts.stop()


def internet(command, opts):
    if command.startswith(opts['youtybe']):
        for x in opts['youtybe']:
            command = command.replace(x, " ").strip()
        url = "https://www.youtube.com/results?search_query=" + command
        webbrowser.get().open(url)
    elif command == opts['music']:
        url = "https://music.yandex.ru/home"
        webbrowser.get().open(url)
def time(command, opts):
    if command in opts['time']:
        now = datetime.now()
        speac(f'Сейчас {now.hour} часов {now.minute} минут')

    elif command in opts['anecdot']:
        x = open('anecdoti.txt', "r", encoding = 'utf8')
        text = x.readlines()
        speac(random.choice(text))
    else:
        internet(command, opts)



def hello(command, opts):
    if command in opts["hello"]:
        speac("Здравствуй Роман!")
    elif command in opts["goodbuy"]:
        speac("Досвидание Роман")
    else:
        time(command, opts)




r = speech_recognition.Recognizer()
with speech_recognition.Microphone() as mic:
    if __name__ == "__main__":
        while True:
                try:
                    print("Я слушаю ")
                    audio = r.listen(mic)
                    query = r.recognize_google(audio, language = "ru-RU").lower()
                    start(query, opts)
                except speech_recognition.UnknownValueError:
                    pass


start(text, opts)




def speac(text):
    tts.say(text)
    tts.runAndWait()
    tts.stop()